FROM debian:stable-slim as builder

ARG NOMAD_VERSION

RUN apt-get update \
    && apt-get install -y \
    unzip \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ADD https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip .
ADD https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_SHA256SUMS .

RUN sha256sum --ignore-missing -c nomad_${NOMAD_VERSION}_SHA256SUMS \
    && unzip nomad_${NOMAD_VERSION}_linux_amd64.zip \
    && chmod +x nomad

FROM debian:stable-slim

RUN apt-get update \
    && apt-get install -y \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /app/nomad /bin/nomad

COPY ./nomad-job /bin/nomad-job
RUN chmod +x /bin/nomad-job
